import React from 'react'
import ReactDOM from'react-dom'
import { BrowserRouter, Route, NavLink } from 'react-router-dom'
import './App.css';




const Home=()=>(
<h1>HoMe</h1>
)


const About=()=>(
<h1>AbOut</h1>
)


const Links=()=>(
    //Replace <ul> with <div> tag and provide styling for <Links> custom tag. Remove <li> as we removed <ul> and add className and remove selected with active in <NavLink>
//<ul>
<div className="list-group">
{/* <li> */}

<NavLink className="list-group-item" exact activeClassName="active" to="/" >HOME</NavLink>    {/* <----Bootstrap*/}

{/* </li> */}
{/* <li>*/}

<NavLink className="list-group-item" activeClassName="active" to="/about">ABOUT</NavLink>


{/* </li>  */}
</div>
//</ul>
)



const App=()=>(
<BrowserRouter>
<div className="row"> {/* <----Bootstrap*/}

<section className="col-sm-4">   {/* <----Bootstrap*/}
<Links/>
</section>

<section className="col-sm-8" >   {/* <----Bootstrap*/}
<Route exact path="/" component={Home}/> 
<Route path="/about" component={About}/> 
</section>

</div>
</BrowserRouter>

)

export default App;

/*Here, we will be using bootstrap in index.html(public->index.html->header section->paste 
link tag copied from bootstrap url) page so as make styling 
better- https://v4-alpha.getbootstrap.com/ . Check index.html. So, now bootstrap is available.
Now, in div tag in App.js we can give className */
