import React from 'react'
import ReactDOM from'react-dom'
import { BrowserRouter, Route, NavLink } from 'react-router-dom'
import './App.css';


const Home=()=>(
    <h1>HoMe</h1>
    )
    
    
const About=()=>(
    <h1>AbOut</h1>
    )
    
    
const Links=()=>(
    <div className="list-group">
    <NavLink className="list-group-item" exact activeClassName="active" to="/" >HOME</NavLink>   
    <NavLink className="list-group-item" activeClassName="active" to="/about">ABOUT</NavLink>
    <NavLink className="list-group-item" activeClassName="active" to="/content">CONTENT</NavLink>
    </div>
    )


const Content=()=>(
// To create links again do same as you did in <Links> tag to create HOME, ABOUT, CONTENT links: <NavLink className="list-group-item" exact activeClassName="active" to="/" >HOME</NavLink>   
//NavLink is just for creating links and providing navigation btwn them and Route tag tells which component to render on that particular link page. Whenever NavLink comes it is accompanied by Route tag.
<div>
<NavLink className="list-group-item" exact activeClassName="active" to="/content/city" >CITY</NavLink>   
<NavLink className="list-group-item" exact activeClassName="active" to="/content/sports" >SPORTS</NavLink>   
<Route path="/content/:contentName" component={ContentDetails}/>
</div>
)
// In <Route path="/content/:contentName" component={contentName}/> --> in path in  /content/:contentName, :contentName means which content out of city or sports will come. It will come dynamically from contentName component class and accordingly that contentName will be appended to the path dynamically.     
    

const ContentDetails=(props)=>(
//<div>{props.match.params.contentName}</div> //as contentname changes so it displays whatever contentName is dynamically coming under match->params->contentName bcs of :contentName. 
//We will append the contentName coming from {props.match.params.contentName} to the lorempixel's src link. We will use ternary operator bcs contentName might return null as well so if contentName is one which is coming from props then diaplay appended url of lorempixel's img url else null.
<div>
{props.match.params.contentName? <div><img src={'lorempixel.com/400/200/'+props.match.params.contentName+'/1/'}/></div>:null}</div>
)

const App=()=>(
    <BrowserRouter>
    <div className="row"> 
    
    <section className="col-sm-4">   
    <Links/>
    </section>
    
    <section className="col-sm-8" >  
    <Route exact path="/" component={Home}/> 
    <Route path="/about" component={About}/> 
    <Route path="/content" component={Content}/>
    </section>
    
    </div>
    </BrowserRouter>
    
    )
    
    export default App;
    

    /*Here, we will see Nested routing concept, so for that we will create one more component 
    called content. When we will click on 'content' it will open content page and it will again 
    contain two links/tabs city and sports. On clicking on city it will display city image and on 
    clicking on sports it will display sports image. This is called as Nested Routing and to achieve 
    this kind of styling we have used lorempixel. If we hit url.....city/1-> it will display city 
    image and if we do ......./sports/1-> it will display sports image. */

    /*For using bootstrap, just put a link tag in header and it is available to be used in js files */