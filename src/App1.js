import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter /*as Router*/, Route, Link, NavLink } from 'react-router-dom'; // Here BrowserRouter we have taken. In react there are lots of different kinds of routers; one of them is BrowserRouter and we have aliased it to name Router

/*To learn about react routing concept we need to first install react router dom in our 
project's package.json- https://www.npmjs.com/package/react-router-dom. We can chk the same by 
going to left hand side explorer and click on package.json and chk dependencies section - there 
you'll find react router dom deependency added. We will start by creating a stateless functional 
component in App.js*/
/*This stateless component will have a return method in which we will return <BrowserRouter> 
component and BrowserRouter in turn will tell which Route to be navigated or linked with this 
component. <Route> component of react takes two things- 
1. Which PATH to route to (here we are giving / which tells we want to route it to starting of this app) and 
2. which COMPONENT  needs to be routed (here we are giving Home component- ome component is a stateless fxnl component ).  */

//Cretaing Home component
const Home=()=>(
<h1>Home</h1>
)


//Creating About component
const About=()=>(
<h1>About </h1>
)

//Creating Links component. this component will have Links for all the component navigation.
const Links=()=>(
/* Inside this, call <Link> tag which you imported from react-router-dom. This <Link> tag takes 
'to' Path(the url on clicking which it should go to home page and about page) to where we need to 
redirect the specific component's link. You can give any name to the link and also you can display 
the link as unoredered list format. Lastly, Don't forget to call this <Links> custom component which
contains all the info about Linking/navigating  inside Apps Component. 
But you can not apply any css styling class to this <Link> tag which we imported from react-router-dom. 
In order to be able to apply css we need to import <navLink> tag from react-router-dom and use this 
tag in place of <Link> tag. You can mention the css class inside App.css which will be called for 
styling in <navLink> tag. Also, import that css in your current javascript page.
activeClassName="selected" tells that whichever component's link is currently clicked on that link selected class which is  
mentioned in App.css page will be dynamically applied. Check App.css for details. */ 
// Since when /about is clicked then it is taking both as red so again here also we need to use 'exact' keyword before activeClassName.
<ul>
<li><NavLink exact activeClassName="selected"  to="/">HOME</NavLink></li>  
<li><NavLink   activeClassName="selected"  to="/about">ABOUT</NavLink></li>
</ul>



/*Alternate---->
<ul>
<li ><Link to="/">   HOME   </Link></li>
<li ><Link to="/about">   ABOUT    </Link></li> 
</ul>
*/
)



//Creating App Component
const App = ()=>( // component App is created here
  //BrowserRouter can return only one child element so we need to put it inside one <div> tag. Jjust like we did inside return(). So creating a <div> tag inside <BrowserRouter>.
    <BrowserRouter> 
    <div>

    <Links/> 
    <Route   exact  path="/"   component={Home}/>  {/*component Home is a stateless fxnl component created here and returned inside <Route> tag*/}
    <Route   path="/about"  component={About} />
    </div>
    </BrowserRouter>
    
    
    /*But here it will display both; the contents of Home as well as of About when 
    localhost:3000/about is hit on the browser bcs it is getting / so it will route to Home component and it is 
    also getting /about so it will also route to About component i.e: '/' is included in both; in path of home and in path of About.  We can resolve this by putting 'exact' keyword infornt of path which we want to be exact(Here, path of home).
    */

    /*Till now before using the <link> tag we were navigating/switching between two pages by going
    to browser and typing corresponding path in the address bar. But thisis not the correct way of 
    doing that rather we would need to use <link> tag for the same. For this, we need to import 
    Link from react-router-dom. create a custom component called Links and inside <BrowserRouter> tag, call this custom component inside <div> tag of render() mtd  */
  
)


export default App;
